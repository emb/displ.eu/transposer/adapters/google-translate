# general use imports
from requests.exceptions import HTTPError
import logging
from cerberus import Validator
from google.cloud import translate


# loads config etc.
from base_adapter import BaseAdapter

# payload params
INPUT =  "input"
MARKUP = "markup"
SOURCE_LANGUAGE_ID = "source_language_id"
TARGET_LANGUAGE_ID = "target_language_id"

CAPTION_ID = "caption_id"
UNIQUE_HASH = "uniqueHash"


PAYLOAD_SCHEMA = {
    INPUT: {'type': 'string', 'required': True},
    SOURCE_LANGUAGE_ID: {'type': 'string'},
    TARGET_LANGUAGE_ID: {'type': 'string', 'required': True},
    MARKUP: {'type': 'string'},
}

TEXT_LIMIT = 30720
CLOSING_TAG = "</x>"

class Adapter(BaseAdapter):
    def __init__(self, connector, config_path):
        super(Adapter, self).__init__(connector, config_path)
        print("Google Translate Adapter ... config:", self.config)

        self.client = translate.TranslationServiceClient()
        project_id = self.config.get("google_translate").get("project_id")
        location = "global"
        self.parent = f"projects/{project_id}/locations/{location}"

    def run(self, payload, msg):
        result = {}
        success = False
        try:
            validator = Validator(PAYLOAD_SCHEMA)
            validator.allow_unknown = True
            if validator.validate(payload) is False:
                # validator.errors could be for example {'input': ['required field'], 'format': ['unallowed value ee']}
                logging.error(validator.errors)
                result['success'] = False
                return result

            target_language_id = payload[TARGET_LANGUAGE_ID]

            # optional params - should we create a whole new adapter for the captions and if yes should we make use of the text translate adapter to avoid duplicating code? probably yes
            # it's crucual to keep these before the actual translation which might raise an exception and skip the rest
            if CAPTION_ID in payload:
                result[CAPTION_ID] = payload[CAPTION_ID]
            index_param = "index"
            if index_param in payload:
                result[index_param] = payload[index_param]
            result["language_id"] = target_language_id
            if UNIQUE_HASH in payload:
                result[UNIQUE_HASH] = payload[UNIQUE_HASH]

            source_language_id = None
            if SOURCE_LANGUAGE_ID in payload:
                source_language_id = payload[SOURCE_LANGUAGE_ID]
            markup = None
            if MARKUP in payload:
                markup = payload[MARKUP]

            # TODO What happens when the source language is unknown but google detects it to be
            # the target language? Maybe we should catch that exception and just return the input?`
            if not (source_language_id is not None and source_language_id == target_language_id):
                translated_text = self.translate_text(
                    payload[INPUT],
                    target_language_id,
                    source_language_id=source_language_id,
                    markup=markup,
                )
            else:
                translated_text = payload[INPUT]

            result["translated_text"] = translated_text
            success = True

        except HTTPError:
            logging.exception("HTTPError Exception thrown")
            success = False
        except Exception:
            logging.exception("Exception thrown")
            success = False

        if "response_topic_id" in payload:
            result["topic"] = payload["response_topic_id"]

        result["success"] = success
        return result

    def translate_text(
        self, text, target_language_id, source_language_id=None, markup=None
    ):

        # Detail on supported types can be found here:
        # https://cloud.google.com/translate/docs/supported-formats

        # this will only work for the transcription format so it should be regarded as a workaround
        # also it should be parallelized (perhaps from the generic translate adapter)
        translated_text = ""
        while len(text) >= TEXT_LIMIT:
            # find last closing tag in xml before the TEXT_LIMIT
            first_chunck = text[:TEXT_LIMIT]
            index_of_last_closing_tag = first_chunck.rfind(CLOSING_TAG) + len(CLOSING_TAG)
            # add the text up to the closing tag to the contents list
            translated_text += self.call_api(first_chunck[:index_of_last_closing_tag], target_language_id, source_language_id, markup)
            # update the text with the rest
            text = text[index_of_last_closing_tag:]

        # add the last part which is smaller than TEXT_LIMIT
        translated_text += self.call_api(text, target_language_id, source_language_id, markup)

        return translated_text


    def call_api(self, text, target_language_id, source_language_id=None, markup=None):
        request = {
            "parent": self.parent,
            "contents": [text],
            "target_language_code": target_language_id
        }

        if markup is not None:
            request["mime_type"] = "text/html"
        else:
            request["mime_type"] = "text/plain"

        if source_language_id is not None:
            request["source_language_code"] = source_language_id

        response = self.client.translate_text(request=request)
        return response.translations[0].translated_text


    def adapter_name(self):
        return 'google_translate'